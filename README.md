Data science and big data analytic
=====
HW2
## Task

### Business logic

Application that calculate average salary and abroad trips count statistics about citizens.

Row input format: (passport №, month,  salary) or (passport №, month, number of abroad trips)

Row output format: age category, average salary, average number of abroad trips

### Ingest technology
Sqoop importer

### Storage technology
HDFS

### Computation technology
Spark RDD

### Report includes

- ZIP-ed src folder with your implementation
- Screenshot of successfully executed tests
- Screenshots of successfully executed job and result (logs)
- Quick build and deploy manual (commands, OS requirements etc)
- System components communication diagram (UML or COMET)

## Links

[Build and deploy manual](./BuildAndDeploy.md)

[Screenshots](./screenshots/README.md)

