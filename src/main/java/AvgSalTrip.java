import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;
/*
* Main class of Spark RDD applications
* It calculates average salary and abroad trips count statistics about citizens
 * */
public class AvgSalTrip {

    // input and outpud data filepath
    static String inputFileName, outputFileName;

    /*
    * Class to represent input data about citizens
    * */
    static public class CitizensInfo {
        // citizen age and count of abroad trips
        int age, trips;
        // citizen salary
        double salary;

        //default constructor
        public CitizensInfo(){
            age = 0;
            trips = 0;
            salary = 0.0f;
        }

        //constructor with 3 parameters
        public CitizensInfo(int age, double salary, int trips){
            this.age = age;
            this.salary = salary;
            this.trips = trips;
        }

        //constructor with string parameter. It will parse comma separated string
        public CitizensInfo(String commaSepLine) {
            String[] values = commaSepLine.split(",");
            age = Integer.parseInt(values[0]);
            trips = Integer.parseInt(values[2]);
            salary = Double.parseDouble(values[1]);
        }

        //getter for age parameter
        public int getAge(){
            return age;
        }

        //getter for trips parameter
        public int getTrips(){
            return trips;
        }

        //getter for salary parameter
        public double getSalary() {
            return salary;
        }

    }

    /*
    * Class to represent output data
    * */
    static public class CitizensAvgInfo{
        int ageCategory, avgTrips;
        double avgSalary;

        CitizensAvgInfo(){
            ageCategory = 0;
            avgTrips = 0;
            avgSalary = 0.0f;
        }

        CitizensAvgInfo(int ageCategory, double avgSalary, int avgTrips){
            this.ageCategory = ageCategory;
            this.avgSalary = avgSalary;
            this.avgTrips = avgTrips;
        }

        public String toString(){
            String result = "";
            switch (ageCategory){
                case 0:
                    result += "< 20";
                    break;
                case 1:
                    result += "20-29";
                    break;
                case 2:
                    result += "30-44";
                    break;
                case 3:
                    result += "45-59";
                    break;
                case 4:
                    result += "> 60";
                    break;
                default:
                    break;
            }
            result += ',';
            result += String.format("%8.2f", avgSalary);
            result += ',';
            result += avgTrips;
            return result;
        }

    }

    static JavaRDD<CitizensInfo> getLines(JavaSparkContext context){
        return context.textFile(inputFileName).map(s -> new CitizensInfo(s));
    }

    private static int ageToAgeCategory(int age){
        int ageCategory = 4;
        if (age < 20)
            return 0;
        if (age < 30)
            return 1;
        if (age < 45)
            return 2;
        if (age < 60)
            return 3;
        return ageCategory;
    }

    static JavaRDD<CitizensAvgInfo> countAvg(JavaRDD<CitizensInfo> info){
        JavaPairRDD<Integer,Double> avgSalaryForAge = info
                .mapToPair(cInfo -> new Tuple2<>(new Integer(ageToAgeCategory(cInfo.getAge())), new Double(cInfo.getSalary())))
                .mapValues(sal -> new Tuple2<>(new Double(sal), new Integer(1)))
                .reduceByKey((t1, t2) -> new Tuple2<>(new Double(t1._1+t2._1), new Integer(t1._2 + t2._2)))
                .mapValues(t -> t._1 / t._2);


        JavaPairRDD<Integer,Integer> avgTripsForAge = info.
                mapToPair(cInfo -> new Tuple2<>(new Integer(ageToAgeCategory(cInfo.getAge())), new Integer(cInfo.getTrips())))
                .mapValues(sal -> new Tuple2<>(new Integer(sal), new Integer(1)))
                .reduceByKey((t1, t2) -> new Tuple2<>(new Integer(t1._1+t2._1), new Integer(t1._2 + t2._2)))
                .mapValues(t -> t._1 / t._2);

        return avgSalaryForAge
                .join(avgTripsForAge)
                .map(t -> new CitizensAvgInfo(t._1, t._2._1, t._2._2));

    }

    public static void main(String[] args){
        //Get spark master url from environment variables
        String sparkMasterUrl = System.getenv("SPARK_MASTER_URL");
        if(sparkMasterUrl == null || sparkMasterUrl == "")
        {
            //If required environment variable is not set, throw exception ending the application
            throw new IllegalStateException("SPARK_MASTER_URL environment variable must be set.");
        }
	
	    //Get hadoop master url from environment variables
        String hadoopMasterUrl = System.getenv("HADOOP_MASTER_URL");
        if(hadoopMasterUrl == null || hadoopMasterUrl == "")
        {
            //If required environment variable is not set, throw exception ending the application
            throw new IllegalStateException("HADOOP_MASTER_URL environment variable must be set.");
        }
	
	    inputFileName = hadoopMasterUrl+"/avgSalTrip/input";
	    outputFileName = hadoopMasterUrl+"/avgSalTrip/output";

        SparkConf sparkConf = new SparkConf().setAppName("avg-salary-trip").setMaster(sparkMasterUrl);

        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        JavaRDD<CitizensInfo> info = getLines(sparkContext);

        JavaRDD<CitizensAvgInfo> counts = countAvg(info);

        counts.map(v -> v.toString()).saveAsTextFile(outputFileName);
    }
}
