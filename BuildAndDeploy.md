Build and deploy manual
=======================

## Required tools

- jdk (version 7 or later)
- maven
- docker
- git

## Preparation

Create working directory

```
mkdir spark-avg-saltrip
cd spark-avg-saltrip
```

Download sources

```
git clone https://bitbucket.org/DaemnKus/ds_bda_hw2
git clone https://github.com/kiwenlau/hadoop-cluster-docker
```

Download hadoop docker image and create hadoop network

```
docker pull kiwenlau/hadoop:1.0
docker pull bde2020/spark-master:2.2.0-hadoop2.7
docker pull bde2020/spark-worker:2.2.0-hadoop2.7
docker pull mysql
docker network create --driver=bridge hadoop
```

## Build

Build application with maven

```
cd ds_bda_hw2
mvn package
cd ..
```

Alternatively : [download jar file](https://bitbucket.org/DaemnKus/ds_bda_hw2/downloads/hw2-1.0.jar)

## Deploy

### Prepare containers
Start spark and mysql containers

```
cd scripts/spark-cluster-docker
./start-container.sh
cd ../..
```
Start docker containers

```
cd hadoop-cluster-docker
./start-container.sh
```
You should receive the following output and get into master's shell

```
start hadoop-master container...
start hadoop-slave1 container...
start hadoop-slave2 container...
```

Start hadoop inside master container

```
./start-hadoop.sh
^D
cd ..
```

### Deploy

Open a new terminal with host shell and cd into `spark-avg-saltrip` directory

Change directory to `scripts`

```
cd ds_bda_hw2
cd scripts
```


Run script prepairing your data. It will copy jar [`sqoop`](./scripts/sqoop) directory with Apache Sqoop, create datadase and insert data into it, set environment variables and import data from mysql to hdfs by sqoop
```
./prepareData.sh
```

### Test / Run

Switch to hadoop master container's shell

Run script [`hw2.sh`](./scripts/hw2.sh)
```
./hw2.sh
```
It will start application