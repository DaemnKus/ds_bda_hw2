#!/bin/bash

docker rm -f mysql &> /dev/null
docker rm -f spark-master &> /dev/null
docker rm -f spark-worker-1 &> /dev/null
docker rm -f spark-worker-2 &> /dev/null
docker rm -f spark-worker-3 &> /dev/null

docker run --name spark-master --network hadoop -h spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-master:2.2.0-hadoop2.7
docker run --name spark-worker-1 --network hadoop --link spark-master:spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-worker:2.2.0-hadoop2.7
docker run --name spark-worker-2 --network hadoop --link spark-master:spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-worker:2.2.0-hadoop2.7
docker run --name spark-worker-3 --network hadoop --link spark-master:spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-worker:2.2.0-hadoop2.7
docker run --name mysql --network hadoop -e MYSQL_ROOT_PASSWORD=Orion123 -d mysql
