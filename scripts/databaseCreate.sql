CREATE SCHEMA citizensActivity;

CREATE TABLE citizensActivity.citizens(citizenId int unsigned, passport_number varchar(10), birthday DATE, primary key (citizenId), unique(passport_number));
CREATE TABLE citizensActivity.activity(citizenId int unsigned, month int unsigned, salary decimal(8,2), trips TINYINT UNSIGNED,primary key (citizenId, month), foreign key (citizenId) REFERENCES citizensActivity.citizens(citizenId) ON DELETE CASCADE ON UPDATE CASCADE);


