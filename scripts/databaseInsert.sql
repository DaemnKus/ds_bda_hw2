INSERT INTO citizensActivity.citizens VALUES (1, "1234567890", "1876-02-21");
INSERT INTO citizensActivity.citizens VALUES (2, "2345678901", "1973-05-16");
INSERT INTO citizensActivity.citizens VALUES (3, "3456789012", "1952-08-03");

INSERT INTO citizensActivity.activity VALUES (1, 201701, 23000.00, 2);
INSERT INTO citizensActivity.activity VALUES (1, 201803, 123456.75, 16);
INSERT INTO citizensActivity.activity VALUES (2, 201701, 42000.50, 1);
INSERT INTO citizensActivity.activity VALUES (2, 201902, 33500.25, 3);
INSERT INTO citizensActivity.activity VALUES (2, 201806, 56789.25, 10);
INSERT INTO citizensActivity.activity VALUES (3, 201912, 856789.25, 1);
