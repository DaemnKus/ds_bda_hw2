#!/bin/bash

docker cp sqoop hadoop-master:/usr/lib/sqoop
docker exec hadoop-master bash -c 'echo export SQOOP_HOME=/usr/lib/sqoop >> /root/.bashrc'
docker exec hadoop-master bash -c 'echo export PATH=\$PATH:\$SQOOP_HOME/bin >> /root/.bashrc'

cat databaseCreate.sql databaseInsert.sql | mysql -u root -p

docker exec hadoop-master /usr/lib/sqoop/bin/sqoop import --connect jdbc:mysql://mysql/citizensActivity --username root --password Orion123 --query 'SELECT FLOOR(DATEDIFF(CURDATE(),birthday) / 365) AS age, salary, trips from activity A join citizens C on A.citizenId = C.citizenId WHERE $CONDITIONS;' --target-dir /avgSalTrip/input -m 1

export SPARK_MASTER_URL=spark://$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' spark-master):7077
export HADOOP_MASTER_URL=hdfs://$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' hadoop-master):9000

